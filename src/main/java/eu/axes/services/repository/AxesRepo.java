package eu.axes.services.repository;

import java.io.File;

import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.messages.Messages;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.service.MessagesKeys;
import org.ow2.weblab.service.SimpleRepo;


/**
 * This class is an extension of the WebLab simple repository.
 * It just changes the way URI of resource to save (or load) are converted to File path.
 * It aims here at taking into account the particular Axes naming scheme.
 * 
 * @author ymombrun
 * @date 2014-11-21
 */
public class AxesRepo extends SimpleRepo {


	private static final String XML = ".xml";


	private static final String URI_PREFIX = "http://axes-project.eu/";


	/**
	 * The default constructor that creates a SimpleRepository on the provided path.
	 * 
	 * @param path
	 *            The path where xml have to be stored.
	 */
	public AxesRepo(final String path) {
		super(path);
	}


	@Override
	protected String checkGetResourceArgs(final LoadResourceArgs lra) throws InvalidParameterException {
		final String uri = super.checkGetResourceArgs(lra);
		this.checkAxesUriValid(uri);
		return uri;
	}


	@Override
	protected Resource checkSaveResourceArgs(final SaveResourceArgs sra) throws InvalidParameterException {
		final Resource res = super.checkSaveResourceArgs(sra);
		this.checkAxesUriValid(res.getUri());
		return res;
	}


	@Override
	protected File uriToFile(final String usageContext, final String uri) {
		final String[] parts = uri.replace(AxesRepo.URI_PREFIX, "").split("/");

		// Create a folder per collection with a folder per video that will only contain the xml. (Can be used to merge content manager and repo)
		final File folder = new File(new File(this.repoBase, parts[0]), parts[1]);
		if (!folder.exists() && !folder.mkdirs()) {
			this.logger.warn(Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_SUB_FOLDER_ERR_2, folder.getAbsolutePath(), uri));
		}

		return new File(folder, parts[1] + AxesRepo.XML);
	}


	/**
	 * Checks that the given URI is valid against AXES naming scheme
	 * 
	 * @param uri
	 *            The uri to be checked
	 * @throws InvalidParameterException
	 *             If uri does not start with <code>"http://axes-project.eu/"</code> or if the second part of the URI does not contain a video and a collection id.
	 */
	protected void checkAxesUriValid(final String uri) throws InvalidParameterException {
		if (!uri.startsWith(AxesRepo.URI_PREFIX)) {
			final String msg = "URI (\" + uri + \") does not start with " + AxesRepo.URI_PREFIX + ".";
			this.logger.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		if (uri.replace(AxesRepo.URI_PREFIX, "").split("/").length != 2) {
			final String msg = "URI (" + uri + ") does not contain collection and video ids.";
			this.logger.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
	}


}
