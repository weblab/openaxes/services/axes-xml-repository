package eu.axes.services.repository;

import java.io.File;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.service.AbstractRepository;
import org.ow2.weblab.service.SimpleRepo;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

@SuppressWarnings({ "javadoc", "static-method" })
public class AxesRepoUnitTest {


	@Test
	public void testOnVariousCollections() throws Exception {

		final AxesRepo repo = new AxesRepo("target/repo");
		for (final File f : FileUtils.listFiles(new File("src/test/resources/res"), new String[] { "xml" }, false)) {
			final Resource res = new WebLabMarshaller().unmarshal(f, Resource.class);
			final String uri1 = res.getUri();
			final SaveResourceArgs sra = new SaveResourceArgs();
			sra.setResource(res);
			final String uri2 = repo.saveResource(sra).getResourceId();
			Assert.assertEquals(uri1, uri2);
			final LoadResourceArgs lra = new LoadResourceArgs();
			lra.setResourceId(uri1);
			final String uri3 = repo.loadResource(lra).getResource().getUri();
			Assert.assertEquals(uri1, uri3);
		}
	}


	@Test
	public void tryToLoadCXF() throws Exception {
		XmlBeanFactory xmlBeanFactory = new XmlBeanFactory(new FileSystemResource("src/main/webapp/WEB-INF/cxf-servlet.xml"));
		final Map<String, AxesRepo> axesRepos = xmlBeanFactory.getBeansOfType(AxesRepo.class);
		Assert.assertNotNull(axesRepos);
		Assert.assertEquals(2, axesRepos.size());

		final Map<String, SimpleRepo> simpleRepos = xmlBeanFactory.getBeansOfType(SimpleRepo.class);
		Assert.assertNotNull(simpleRepos);
		Assert.assertEquals(2, simpleRepos.size());

		final Map<String, AbstractRepository> abstractRepos = xmlBeanFactory.getBeansOfType(AbstractRepository.class);
		Assert.assertNotNull(abstractRepos);
		Assert.assertEquals(2, abstractRepos.size());
	}

}
